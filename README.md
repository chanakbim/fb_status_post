# Basic Facebook Status Update  #

This project uses Symfony Framework,php and javascript for viewing user info and update status on facebook(Repository file name fb_status_update)


### How to use the Application ###


* On main screen click on login with facebook button for logging in.
* If You are already logged in other tab then on clicking the login with facebook button, will ask you only for giving permission to access your facebook account.
* After logging in there are two button "your data on facebook" and "post on facebook button"
* On clicking the "your data on facebook" button you will view your basic info on facebook.
* For posting your status,fill the status form (textarea) and click on "post to facebook" button.
* In header your all activities responses are shown.


###Thanking You for going through it###